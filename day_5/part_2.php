<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$seats = array_fill(0,818,false);

while($line = fgets($fp)) {
    $line = trim($line);
    if (empty($line)) continue;

    $row = substr($line,0,7);
    $seat = substr($line,-3);

    $row = str_replace("B","1",$row);
    $row = str_replace("F","0",$row);
    $row = bindec($row);

    $seat = str_replace("L","0",$seat);
    $seat = str_replace("R","1",$seat);
    $seat = bindec($seat);

    $seats[$row * 8 + $seat] = true;
}

$hasFoundTrue = false;

foreach($seats as $key => $seat) {
    if($seat) {
        $hasFoundTrue = true;
    } elseif($hasFoundTrue) {
        echo "Result: {$key}\n";
        die();
    }
}

