<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$adapters = [0];

while($line = fgets($fp)) {
    $adapters[] =intval(trim($line));
}

sort($adapters);

$cache = [
    count($adapters) - 1 => 1
];

function findPaths($position = 0) {
    global $adapters, $cache;

    if(array_key_exists($position,$cache)) {
        return $cache[$position];
    }

    $result = 0;
    $adapter = $adapters[$position];
    for($i = 1; $position + $i < count($adapters) && $adapters[$position + $i] - $adapter < 4; $i++) {
        $result += findPaths($position + $i);
    }
    $cache[$position] = $result;
    return $result;
}

for($i = count($adapters) - 1; $i > 0; $i--) {
    findPaths($i);
}

$result = findPaths(0);

echo "Result: {$result}\n";

