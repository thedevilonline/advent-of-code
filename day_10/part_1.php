<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$adapters = [];

while($line = fgets($fp)) {
    $adapters[] =intval(trim($line));
}

sort($adapters);

$diffs = [
    0 => 0,
    1 => 0,
    2 => 0,
    3 => 1,
];

$current = 0;
foreach($adapters as $adapter) {
    $diffs[$adapter - $current]++;
    $current = $adapter;
}

var_dump($diffs);
$result = $diffs[1] * $diffs[3];
echo "Result: {$diffs[1]}*{$diffs[3]}={$result}";
