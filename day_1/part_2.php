<?php
declare(strict_types=1);


function sortInteger(int &$a, int &$b) {
    return $a <=> $b;
}

$fp = fopen("input.txt","r");

$data = [];
while($line = fgets($fp)) {
    if(empty($line)) {
       continue;
    }
    $data[] = intval($line);
}

usort($data,"sortInteger");

for($i = 0; $i < count($data); $i++) {
    for($j = $i + 1; $j < count($data); $j++) {
        for($k = count($data) - 1; $k > $j; $k--) {
            if($data[$i] + $data[$j] + $data[$k] < 2020) {
                break;
            }

            if($data[$i] + $data[$j] + $data[$k] == 2020) {
                $result = $data[$i] * $data[$j] * $data[$k];
                echo "Result found: {$data[$i]} + {$data[$j]} + {$data[$k]} = 2020\n";
                echo "Result found: {$data[$i]} * {$data[$j]} * {$data[$k]} = {$result}\n";
                die();
            }
        }
    }
}
