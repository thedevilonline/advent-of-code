<?php
declare(strict_types=1);

function sortInteger(int &$a, int &$b) {
    return $a <=> $b;
}

$fp = fopen("input.txt","r");

$data = [];
while($line = fgets($fp)) {
    $data[] = intval($line);
}

usort($data,"sortInteger");

for($i = 0; $i < count($data); $i++) {
    for($j = count($data) - 1; $j > $i; $j--) {
        if($data[$i] + $data[$j] < 2020) {
            break;
        }

        if($data[$i] + $data[$j] == 2020) {
            $result = $data[$i] * $data[$j];
            echo "Result found: {$data[$i]} + {$data[$j]} = 2020\n";
            echo "Result found: {$data[$i]} * {$data[$j]} = {$result}\n";
            die();
        }
    }
}
