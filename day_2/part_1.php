<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$counter = 0;

while($line = fgets($fp)) {
    if(empty($line)) continue;

    preg_match_all('/([0-9]+)-([0-9]+) ([a-z]+): ([a-z]+)/',$line,$matches);
    $count = substr_count($matches[4][0],$matches[3][0]);

    $valid = $count >= intval($matches[1][0]) && $count <= intval($matches[2][0]);
    if($valid) {
        $counter++;
    }
}

echo "Result: {$counter}\n";
