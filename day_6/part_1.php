<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$result = 0;

$chars = [];

while($line = fgets($fp)) {
    $line = trim($line);
    if (empty($line)) {
        $chars = array_unique($chars);
        $result += count($chars);
        $chars = [];
        continue;
    }

    for($i = 0; $i < strlen($line); $i++) {
        $chars[] = $line[$i];
    }
}

$chars = array_unique($chars);
$result += count($chars);

echo "Result: {$result}\n";
