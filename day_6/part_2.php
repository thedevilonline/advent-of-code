<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$result = 0;

$chars = [];
$lineCount = 0;

while($line = fgets($fp)) {
    $line = trim($line);
    if (empty($line)) {
        foreach($chars as $key => $value) {
            if ($value == $lineCount) {
                $result++;
            }
        }
        $chars = [];
        $lineCount = 0;
        continue;
    }
    $lineCount++;

    for($i = 0; $i < strlen($line); $i++) {
        if(!array_key_exists($line[$i],$chars)) $chars[$line[$i]] = 0;
        $chars[$line[$i]]++;
    }
}

foreach($chars as $key => $value) {
    if ($value == $lineCount) {
        $result++;
    }
}

echo "Result: {$result}\n";
