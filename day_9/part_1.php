<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$preamble = 25;

$numbers = [];

while($line = fgets($fp)) {
    if(empty($line)) continue;

    $line = intval($line);

    if(count($numbers) == $preamble) {
        $sortedNumbers = $numbers;
        rsort($sortedNumbers);
        $found = false;
        for($i = 0; $i < $preamble - 1; $i++) {
            for($j = $i + 1; $j < $preamble; $j++) {
                if($sortedNumbers[$i] == $sortedNumbers[$j]) {
                    continue;
                } elseif($sortedNumbers[$i] + $sortedNumbers[$j] == $line) {
                    $found = true;
                    break 2;
                } elseif($sortedNumbers[$i] + $sortedNumbers[$j] < $line) {
                    break;
                }
            }
        }

        if(!$found) {
            var_dump($sortedNumbers);

            echo "Result: {$line}\n";
            die();
        }
    }

    $numbers[] = $line;

    if(count($numbers) > $preamble) {
        array_shift($numbers);
    }
}

echo "All OK\n";
