<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$preamble = 25;

$allNumbers = [];
$numbers = [];
$result = 0;

while($line = fgets($fp)) {
    if(empty($line)) continue;

    $line = intval($line);
    $allNumbers[] = $line;

    if(count($numbers) == $preamble && $result === 0) {
        $sortedNumbers = $numbers;
        rsort($sortedNumbers);
        $found = false;
        for($i = 0; $i < $preamble - 1; $i++) {
            for($j = $i + 1; $j < $preamble; $j++) {
                if($sortedNumbers[$i] == $sortedNumbers[$j]) {
                    continue;
                } elseif($sortedNumbers[$i] + $sortedNumbers[$j] == $line) {
                    $found = true;
                    break 2;
                } elseif($sortedNumbers[$i] + $sortedNumbers[$j] < $line) {
                    break;
                }
            }
        }

        if(!$found) {
            $result = $line;
        }
    }

    if($result === 0) {
        $numbers[] = $line;

        if (count($numbers) > $preamble) {
            array_shift($numbers);
        }
    }
}
$usedNumbers = [];
for($i = 0; $i < count($allNumbers) - 1; $i++) {
    $usedNumbers = [$allNumbers[$i]];
    $sum = $allNumbers[$i];
    for($j = $i + 1; $j < count($allNumbers); $j++) {
        $usedNumbers[] = $allNumbers[$j];
        $sum += $allNumbers[$j];

        if($sum == $result) {
            break 2;
        } elseif($sum > $result) {
            break;
        }
    }
}

sort($usedNumbers);

$usedNumbersString = implode(" + ",$usedNumbers);
echo "Result: {$result} = {$usedNumbersString}\n";
