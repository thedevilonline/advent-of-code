<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$requiredFields = [
    "byr", // (Birth Year)
    "iyr", // (Issue Year)
    "eyr", // (Expiration Year)
    "hgt", // (Height)
    "hcl", // (Hair Color)
    "ecl", // (Eye Color)
    "pid", // (Passport ID)
];
$optionalFields = [
    "cid", // (Country ID)
];

$result = 0;
$fields = [];
$usedOptionalFields = [];

$valid = true;

while($line = fgets($fp)) {
    $line = trim($line);
    if(empty($line)) {
        if($valid && count($fields) == count($requiredFields) + count($usedOptionalFields)) {
            $result++;
        }
        $valid = true;
        $fields = [];
    } elseif($valid) {
        $data = explode(' ',$line);
        foreach($data as $field) {
            $fieldData = explode(":",$field);
            if(in_array($fieldData[0],$requiredFields)) {
                if(array_key_exists($fieldData[0],$fields)) {
                    $valid = false;
                    break;
                } else {
                    $fields[$fieldData[0]] = $fieldData[1];
                }
            } elseif(in_array($fieldData[0],$optionalFields)) {
                if(array_key_exists($fieldData[0],$fields)) {
                    $valid = false;
                    break;
                } else {
                    $usedOptionalFields[] = $fieldData[0];
                    $fields[$fieldData[0]] = $fieldData[1];
                }
            }
        }
    }


}

echo "Result: {$result}\n";
