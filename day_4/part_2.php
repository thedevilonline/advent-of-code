<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$requiredFields = [
    "byr", // (Birth Year)
    "iyr", // (Issue Year)
    "eyr", // (Expiration Year)
    "hgt", // (Height)
    "hcl", // (Hair Color)
    "ecl", // (Eye Color)
    "pid", // (Passport ID)
];
$optionalFields = [
    "cid", // (Country ID)
];

$result = 0;
$fields = [];
$usedOptionalFields = [];

$valid = true;

while($line = fgets($fp)) {
    $line = trim($line);
    if(empty($line)) {
        if($valid && count($fields) == count($requiredFields) + count($usedOptionalFields)) {
            foreach($fields as $key => $value) {
                if(!$valid) break;
                switch($key) {
                    case "byr": // (Birth Year)
                        if(!is_numeric($value) || intval($value) < 1920 || intval($value) > 2002) $valid = false;
                        break;
                    case "iyr": // (Issue Year)
                        if(!is_numeric($value) || intval($value) < 2010 || intval($value) > 2020) $valid = false;
                        break;
                    case "eyr": // (Expiration Year)
                        if(!is_numeric($value) || intval($value) < 2020 || intval($value) > 2030) $valid = false;
                        break;
                    case "hgt": // (Height)
                        $unit = substr($value,-2);
                        $height = substr($value,0,-2);
                        if($unit == "cm") {
                            if(!is_numeric($height) || intval($height) < 150 || intval($height) > 193) $valid = false;
                        } elseif($unit == "in") {
                            if(!is_numeric($height) || intval($height) < 59 || intval($height) > 76) $valid = false;
                        } else {
                            $valid = false;
                        }
                        break;
                    case "hcl": // (Hair Color)
                        if(preg_match("/^#[a-f0-9]{6}$/",$value) !== 1) $valid = false;
                        break;
                    case "ecl": // (Eye Color)
                        if(!in_array($value,["amb","blu","brn","gry","grn","hzl","oth"])) $valid = false;
                        break;
                    case "pid": // (Passport ID)
                        if(!is_numeric($value) || strlen($value) != 9) $valid = false;
                        break;
                }
            }
            if($valid) $result++;
        }
        $valid = true;
        $fields = [];
        $usedOptionalFields = [];
    } elseif($valid) {
        $data = explode(' ',$line);
        foreach($data as $field) {
            $fieldData = explode(":",$field);
            if(in_array($fieldData[0],$requiredFields)) {
                if(array_key_exists($fieldData[0],$fields)) {
                    $valid = false;
                    break;
                } else {
                    $fields[$fieldData[0]] = $fieldData[1];
                }
            } elseif(in_array($fieldData[0],$optionalFields)) {
                if(array_key_exists($fieldData[0],$fields)) {
                    $valid = false;
                    break;
                } else {
                    $usedOptionalFields[] = $fieldData[0];
                    $fields[$fieldData[0]] = $fieldData[1];
                }
            }
        }
    }


}

echo "Result: {$result}\n";
