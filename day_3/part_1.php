<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$counter = 0;
$result = 0;
while($line = fgets($fp)) {
    $line = trim($line);
    if(empty($line)) continue;

    if($line[$counter] == "#") $result++;
    $counter = ($counter + 3) % strlen($line);
}

echo "Result: {$result}\n";
