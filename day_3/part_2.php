<?php
declare(strict_types=1);

$results = [];
$results[] = checkPath(1,1);
$results[] = checkPath(3,1);
$results[] = checkPath(5,1);
$results[] = checkPath(7,1);
$results[] = checkPath(1,2);

function checkPath($right, $down)
{
    $fp = fopen("input.txt", "r");
    $counter = 0;
    $result = 0;
    while ($line = fgets($fp)) {
        $line = trim($line);
        if (empty($line)) continue;

        if ($line[$counter] == "#") $result++;
        $counter = ($counter + $right) % strlen($line);

        for($i = 1; $i < $down; $i++) {
            fgets($fp);
        }
    }

    return $result;
}

$result = 1;
foreach($results as $intermediateResult) {
    $result *= $intermediateResult;
}

echo "Result: {$result}\n";
