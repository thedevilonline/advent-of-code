<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$bags = [];

while($line = fgets($fp)) {
    $line = str_replace(["contain ",",","."],"",trim($line));
    $words = explode(" ",$line);
    if($words[3] == "no") continue;
    $currentBag = $words[0]." ".$words[1];
    if(!array_key_exists($currentBag,$bags)) {
        $bags[$currentBag] = [];
    }

    for($i = 0; $i < (count($words) - 3) / 4; $i++) {
        $bagCount = $words[$i * 4 + 3];
        $bag = $words[$i * 4 + 3 + 1]." ".$words[$i * 4 + 3 + 2];
        $bags[$currentBag][$bag] = intval($bagCount);
    }
}

function getBagCount($color) {
    global $bags;

    $result = 0;
    if(array_key_exists($color,$bags)) {
        foreach ($bags[$color] as $container => $count) {
            //var_dump($container,$count);
            $result += $count;
            $result += $count * getBagCount($container);
        }
    }

    return $result;

}


$result = getBagCount("shiny gold");
echo "Result: {$result}\n";
