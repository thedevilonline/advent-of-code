<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$bags = [];

while($line = fgets($fp)) {
    $line = str_replace(["contain ",",","."],"",trim($line));
    $words = explode(" ",$line);
    if($words[3] == "no") continue;
    $currentBag = $words[0]." ".$words[1];


    for($i = 0; $i < (count($words) - 3) / 4; $i++) {
        $bagCount = $words[$i * 3 + 3];
        $bag = $words[$i * 4 + 4]." ".$words[$i * 4 + 5];
        if(!array_key_exists($bag,$bags)) {
            $bags[$bag] = [];
        }
        $bags[$bag][] = $currentBag;
    }
}

function getContainerBags($color) {
    global $bags;

    $result = [];
    if(array_key_exists($color,$bags)) {
        foreach ($bags[$color] as $container) {
            $result[$container] = $container;
            $result = array_merge($result, getContainerBags($container));
        }
    }

    return $result;

}


$containerBags = getContainerBags("shiny gold");
$result = count($containerBags);
echo "Result: {$result}\n";
