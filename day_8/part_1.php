<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$commands = [];

while($line = fgets($fp)) {
    $commands[] = explode(" ",trim($line));
}

$acc = 0;
$pointer = 0;
$oldPointers = [];

while(true) {
    if(in_array($pointer,$oldPointers)) break;

    $oldPointers[] = $pointer;

    $command = $commands[$pointer];
    switch($command[0]) {
        case 'nop':
            $pointer++;
            break;
        case 'acc':
            $acc += intval(str_replace("+","",$command[1]));
            $pointer++;
            break;
        case 'jmp':
            $pointer += intval(str_replace("+","",$command[1]));
            break;
        default:
            throw new \Exception("Unknown command {$command[0]}");
    }
}

echo "Result: {$acc}\n";
