<?php
declare(strict_types=1);

$fp = fopen("input.txt","r");

$commands = [];

while($line = fgets($fp)) {
    $commands[] = explode(" ",trim($line));
}

function calculateAcc($commands) {
    $acc = 0;
    $pointer = 0;
    $oldPointers = [];

    while(true) {
        if($pointer >= count($commands)) break;
        if(in_array($pointer,$oldPointers) || $pointer >= count($commands)) return false;

        $oldPointers[] = $pointer;

        $command = $commands[$pointer];
        switch($command[0]) {
            case 'nop':
                $pointer++;
                break;
            case 'acc':
                $acc += intval(str_replace("+","",$command[1]));
                $pointer++;
                break;
            case 'jmp':
                $pointer += intval(str_replace("+","",$command[1]));
                break;
            default:
                throw new \Exception("Unknown command {$command[0]}");
        }
    }

    return $acc;
}

$accResult = false;

foreach($commands as $key => $command) {
    switch($command[0]) {
        case 'nop':
            $commands[$key][0] = "jmp";
            $accResult = calculateAcc($commands);
            $commands[$key][0] = "nop";
            break;
        case 'jmp':
            $commands[$key][0] = "nop";
            $accResult = calculateAcc($commands);
            $commands[$key][0] = "jmp";
            break;
    }
    if($accResult !== false) break;
}

echo "Result: {$accResult}\n";
